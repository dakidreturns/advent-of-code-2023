import io

def create_3_tuple(input_file: io.TextIOWrapper) -> list[tuple[int|str]] :
    three_tuples = []
    line = input_file.readline()
    raw_table = []
    y=-1
    while not (line is None or line.strip() == ''):
        y+=1
        for x,char in enumerate(line):
            if char != '.' and char !='\n':
                try: 
                    char_val = int(char)
                except ValueError:
                    char_val = char
                three_tuples.append((x,y,char_val))
        raw_table.append(line)
        line = input_file.readline()
    return three_tuples, raw_table

def create_number_list(three_tuple: list[tuple[int|str]]
                       ) -> list[tuple[tuple[int]|int|str]]:
    detecting_num = False
    num_list = []
    temp_num=0
    for x,y,char in three_tuple:
        if isinstance(char,int):
            if not detecting_num:
                detecting_num = True
                num_coord = (x,y)
            elif x != prev_ind+1:
                length = prev_ind - num_coord[0] + 1
                num_list.append((num_coord,length,temp_num))
                temp_num = 0
                num_coord = (x,y)
            temp_num = temp_num*10 + char
            prev_ind = x
        
        elif detecting_num:
            length = prev_ind - num_coord[0] + 1
            num_list.append((num_coord,length,temp_num))
            temp_num = 0
            detecting_num = False

    if detecting_num:
        length = prev_ind - num_coord[0] + 1
        num_list.append((num_coord,length,temp_num))
        temp_num = 0

    return num_list

def find_symbols(start_coord: tuple[int], length: int, raw_data: list[list[str]]):
    row = len(raw_data)
    col = len(raw_data[0])
    
    count = 0
    start_x = max(0, start_coord[0]-1)
    end_x = min (start_x + length + 2, col)

    start_y = max(0,start_coord[1]-1)
    end_y = min(row, start_coord[1] + 2)

    for x in range(start_x, end_x):
        for y in range(start_y, end_y):
            if (raw_data[y][x] != '.' and raw_data[y][x] != '\n') and not raw_data[y][x].isdigit():
                count+=1
    return count

def part1_task(number_list:list, raw_data: list):
    output = 0
    for start_coord, length, number in number_list:
        count = find_symbols(start_coord, length, raw_data)

        output += (count*number)
    return output
    

if __name__ == "__main__":
    input_file = open("C:/Users/Rohith Karunakaran/Personal/Advent Of Code 2023/inputs/day3/input.txt")
    # input_file = open("C:/Users/Rohith Karunakaran/Personal/Advent Of Code 2023/python/day3/test.txt")
    threeTupleList,raw_table = create_3_tuple(input_file)
    numberList = create_number_list(threeTupleList)
    for threTup in threeTupleList:
        print(threTup)
    # print(threeTupleList)
    print(len(numberList))
    print(part1_task(numberList,raw_table))