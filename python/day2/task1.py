'''
The Elf would first like to know which games would have been possible if the bag contained only 12 red cubes, 13 green cubes, and 14 blue cubes?
'''

import io
from functools import reduce


def strip_white_space(line:str):
    return line.strip()

def get_games_string(line):
    return tuple(map(strip_white_space,line.split(':')))

def get_games_list(game_list_string) -> list[str]:
    return list(map(strip_white_space, game_list_string.split(";")))

def get_color_number_map(game_str:str) -> dict:
    color_number_map = {}
    for color_number_string in list(map(strip_white_space,game_str.split(','))):
        number_and_color = tuple(map(strip_white_space,color_number_string.split(' ')))
        number, color = number_and_color
        color_number_map[color] = number
    return color_number_map

def check_map(game_map:dict[str, str])->bool:
    red_count = game_map.get('red') 
    green_count = game_map.get('green') 
    blue_count = game_map.get('blue') 
    
    if not red_count is None  :
        if int(red_count) > 12:
            return False
    if not blue_count is None:
        if int(blue_count) > 14:
            return False
    if not green_count is None:
        if int(green_count) > 13:
            return False
    return True

def get_game_name_and_list(line:str) -> tuple[ int, list[ dict[str,str] ]]:
    game_name, games_list_string = get_games_string(line)

    game_number = int(game_name.split(' ')[-1])

    game_list = get_games_list(games_list_string)
    game_map_list = []
    for game in game_list:
        ball_map = get_color_number_map(game)
        game_map_list.append(ball_map)
    return game_name,game_map_list

def create_game_tuple(game_dict: dict):
    '''
        Number of (red,green,blue)
    '''
    game_tuple = [0,0,0]

    for game_key, square_num in game_dict.items():
        if game_key == 'red':
            game_tuple[0] = int(square_num)
        elif game_key == 'green':
            game_tuple[1] = int(square_num)
        elif game_key == 'blue' :
            game_tuple[2] = int(square_num)
    return game_tuple

def update_game_tuple(temp_list: list[int], min_amount_tuple: list[int]) -> list[int]:
    return [ max(x,y) for x,y in zip(temp_list,min_amount_tuple)]

def find_set_power(item_amount: list):
    item_amount = [x if x > 1 else 1 for x in item_amount]
    return reduce(lambda x,y: x*y, item_amount)


def part2_task(input_file:io.TextIOWrapper):
    final = 0
    line = input_file.readline()
    while not (line is None or line.strip() == ''):
        min_amount_tuple = [0,0,0]
        game_number, game_maps = get_game_name_and_list(line)
        for game_map in game_maps:
            temp_list = create_game_tuple(game_dict=game_map)
            min_amount_tuple = update_game_tuple(temp_list, min_amount_tuple)
        final += find_set_power(min_amount_tuple)
        line = input_file.readline()

    return final


def part1_task(input_file:io.TextIOWrapper) -> int:
    final = 0
    line = input_file.readline()
    while not (line is None or line.strip() == ''):
        flag = True

        game_number, game_maps = get_game_name_and_list(line)
        for game_map in game_maps:
            if not check_map(game_map) :
                flag = False

        if flag:
            final += game_number

        line = input_file.readline()
    return final

if __name__=="__main__":
    input_file = open("C:/Users/Rohith Karunakaran/Personal/Advent Of Code 2023/inputs/day2/task1.txt")

    part1_result = part1_task(input_file=input_file)
    print(part1_result)

    print(part2_task(input_file))