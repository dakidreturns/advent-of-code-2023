import re

part1_regex = r"([0-9]+)"
part2_regex = r"(?=([0-9]+)|(?=(one))|(?=(two))|(?=(three))|(?=(four))|(?=(five))|(?=(six))|(?=(seven))|(?=(eight))|(?=(nine)))"

def convert_str_to_num(numbers:list)->list:
    for index,number in enumerate(numbers):
        for item in number:
            if item != '':
                number = item
                numbers[index] = item
        if(number == 'one'):
            numbers[index] = '1'
        elif(number == 'two'):
            numbers[index] = '2'
        elif(number == 'three'):
            numbers[index] = '3'
        elif(number == 'four'):
            numbers[index] = '4'
        elif(number == 'five'):
            numbers[index] = '5'
        elif(number == 'six'):
            numbers[index] = '6'
        elif(number == 'seven'):
            numbers[index] = '7'
        elif(number == 'eight'):
            numbers[index] = '8'
        elif(number == 'nine'):
            numbers[index] = '9'
    return numbers
        


def get_result_part_one(input_file):
    final_sum = 0
    while True:
        line = input_file.readline()
        if line is None or line.strip() == '': 
            break
        numbers = re.findall(part1_regex,line)
        final_sum += int(numbers[0][0])*10 + int(numbers[-1][-1])
    return final_sum

def get_result_part_two(input_file):
    final_sum = 0
    while True:
        line = input_file.readline()
        if line is None or line.strip() == '': 
            break
        numbers = re.findall(part2_regex,line)
        numbers = convert_str_to_num(numbers)
        number_in_line = int(numbers[0][0])*10 + int(numbers[-1][-1])
        final_sum += number_in_line
    return final_sum

if __name__ == "__main__":
    input_file = open("C:/Users/Rohith Karunakaran/Personal/Advent Of Code 2023/inputs/day1/task1.txt")
    part1_result = get_result_part_one(input_file)
    input_file.seek(0)
    part2_result = get_result_part_two(input_file)
    
    print(part1_result)
    
    print(part2_result)